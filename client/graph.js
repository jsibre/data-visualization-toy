Template.graph.helpers({
	datapoints: function() {
		return DataPoints.find({},{sort:{ts:1}});
	},
});

var config = {
	duration : 500,
	records : 100
}


// Function to make SVG responsive-ish, and function to 
// resize it when needed.
// http://www.brendansudol.com/posts/responsive-d3/
function responsivefy(svg) {
    // get container + svg aspect ratio
    var container = d3.select(svg.node().parentNode),
        width = parseInt(svg.style("width")),
        height = parseInt(svg.style("height")),
        aspect = width / height;

    // add viewBox and preserveAspectRatio properties,
    // and call resize so that svg resizes on inital page load
    svg.attr("viewBox", "0 0 " + width + " " + height)
        .attr("perserveAspectRatio", "xMinYMid")
        .call(resize);

    // to register multiple listeners for same event type, 
    // you need to add namespace, i.e., 'click.foo'
    // necessary if you call invoke this function for multiple svgs
    // api docs: https://github.com/mbostock/d3/wiki/Selections#on
    d3.select(window).on("resize." + container.attr("id"), resize);

    // get width of container and resize svg to fit it
    function resize() {
        var targetWidth = parseInt(container.style("width"));
        svg.attr("width", targetWidth);
        svg.attr("height", Math.round(targetWidth / aspect));
    }
}


var setNominal = function() {
	var n = getNumberFrom($("#nominal").val());
	if (n != null) {
		Session.set("nominal", n);
	}
}

var setWarning = function() {
	var n = getNumberFrom($("#warnRange").val());
	if (n != null) {
		Session.set("warnRange", n);
	}
}

var setAlarm = function() {
	var n = getNumberFrom($("#alarmRange").val());
	if (n != null) {
		Session.set("alarmRange", n);
	}
}

var getNumberFrom = function(s) {
	if ($.isNumeric(s)) {
		return Number(s);
	} else {
		return null;
	}
}

Template.graph.events = {
		'change #warnRange' : setWarning,
		'change #alarmRange' : setAlarm,
		'change #nominal' : setNominal,
}

Template.graph.rendered = function(){
	//Width and height
	var w = 500;
	var h = 500;

	setWarning();
	setAlarm();
	setNominal();
	
	var xScale = d3.time.scale().range([0, w]);
	var yScale = d3.scale.linear().range([0, h]);

	yScale.domain([0, 100]);
	
	// Determine if d.value is "good", "warn", or "alarm"
	var classForValue = function(d) {
		if (d.value > Session.get("nominal") + Session.get("alarmRange") || d.value < Session.get("nominal") - Session.get("alarmRange")) {
			return "alarm";
		} else if (d.value > Session.get("nominal") + Session.get("warnRange") || d.value < Session.get("nominal") - Session.get("warnRange")) {
			return "warn";
		} else {
			return "good";
		}
	}

	// Define color codes for "good", "warn", or "alarm"
	var colorCodesForClasses = {
		alarm : {
			solid:"rgb(250, 0, 0)",
			text:"rgb(250, 0, 0)",
			transparent:"rgba(250, 0, 0, 0.8)"
		},
		warn : {
			solid:"rgb(250, 250, 0)",
			text:"rgb(250, 250, 0)",
			transparent:"rgba(250, 250, 0, 0.8)"
		},
		good : {
			solid:"rgb(0, 250, 0)",
			text:"rgb(250, 250, 250)",
			transparent:"rgba(0, 250, 0, 0.4)"
		}
	}


	//Define key function, to be used when binding data
	var getKey = function(d) {
		if (d)
			return d._id;
		else
			return null;
	};
	
	//Create SVG element
	var svg = d3.select("#graph2")
				.attr("width", w)
				.attr("height", h)
				.call(responsivefy);
//				.attr("viewbox", "0 0 " + w + " " + h)
//				.attr("preserveAspectRatio", "xMinYMid");

	// make a background
	svg.append("rect")
		.attr("width", "100%")
		.attr("height", "100%")
		.attr("rx", "20")
		.attr("ry", "20")
		.attr("fill", "rgb(80,80,80)");

	// limit lines
	svg.append("rect")
		.attr('id', 'upperAlarm')
		.attr("width", "100%")
		.attr("height", "1")
		.attr("y", h - yScale(Session.get("nominal") + Session.get("alarmRange")))
		.attr("fill", "rgba(150,0,0, 1)");

	svg.append("rect")
		.attr('id', 'upperWarn')
		.attr("width", "100%")
		.attr("height", "1")
		.attr("y", h - yScale(Session.get("nominal") + Session.get("warnRange")))
		.attr("fill", "rgba(150,150,0, 1)");

	svg.append("rect")
		.attr('id','lowerWarn')
		.attr("width", "100%")
		.attr("height", "1")
		.attr("y", h - yScale(Session.get("nominal") - Session.get("warnRange")))
		.attr("fill", "rgba(150,150,0, 1)");

	svg.append("rect")
		.attr('id','lowerAlarm')
		.attr("width", "100%")
		.attr("height", "1")
		.attr("y", h - yScale(Session.get("nominal") - Session.get("alarmRange")))
		.attr("fill", "rgba(150,0,0, 1)");

	Meteor.subscribe("datapoints", config.records);
	
	Tracker.autorun(function(){
		var dataset = DataPoints.find({},{sort:{ts:1},limit:config.records}).fetch();
		//console.log(DataPoints.find().count());
		var datasetLatest = DataPoints.find({},{sort:{ts:-1},limit:1}).fetch();

		//Update xScale domain : (Now - 30 seconds) to (Now)
		(function() {
			// "now" defaults to actual current time (on client)
			var now = new Date();
			// but inconsistencies between client and server can make the 
			// plot start too far left or right, so better to base
			// it off data from server (if available)
			if (datasetLatest && datasetLatest.length > 0) {
				now = datasetLatest[0].ts;
			}
			var xDom = [now - 30000, now];
			xScale.domain(xDom);
		})();

		/*		
		// This version draws the whole line each time as one path.  
		// There's no chance to color individual segements (that I know of)
		var path = svg.selectAll("path");
		path.remove();

		var lineFunc = d3.svg.line()
			.x(function(d) {return xScale(d.ts); })
			.y(function(d) {return h - yScale(d.value); })
			.interpolate("linear");

		svg.append("path")
			.attr("d", lineFunc(dataset))
			.attr("stroke","white")
			.attr("stroke-width", "2")
			.attr("fill", "none");

		*/

		// Transformed dataset for use by path (needs last datapoint values)
		var prevD = null;
		var dataset2 = dataset.map(function(d) {
			var newD = {
				_id: d._id,
				x: xScale(d.ts),
				y: h - yScale(d.value),
				value: d.value // for benefit of the color coding function
			};
			if (prevD) {
				newD.prevX = prevD.x;
				newD.prevY = prevD.y;
			} else {
				newD.prevX = newD.x;
				newD.prevY = newD.y;
			}
			prevD = newD;
			return newD;
		});



		// Check/Update limit lines
		svg.select('rect#upperAlarm')
			.attr("y", h - yScale(Session.get("nominal") + Session.get("alarmRange")));

		svg.select('rect#upperWarn')
			.attr("y", h - yScale(Session.get("nominal") + Session.get("warnRange")));

		svg.select('rect#lowerWarn')
			.attr("y", h - yScale(Session.get("nominal") - Session.get("warnRange")));

		svg.select('rect#lowerAlarm')
			.attr("y", h - yScale(Session.get("nominal") - Session.get("alarmRange")));




		//
		// Draw the paths
		//
		var paths = svg.selectAll("path").data(dataset2, getKey);

		//Enter…
		paths.enter()
			.append("path")
			.attr("stroke-width", "2")
			.attr("fill", "none")
			.attr("data-id", function(d){
				return d._id;
			});

		//Update…
		paths.attr("stroke", function(d) {
				return colorCodesForClasses[classForValue(d)].solid;
			})
			.attr("d", function(d) { return "M " + d.prevX + " " + d.prevY + " L " + d.x + " " + d.y });

		//Exit…
		paths.exit()
			.remove();




		//
		// Draw the datapoints
		//
		var data = svg.selectAll("circle").data(dataset, getKey);

		//Enter…
		data.enter()
			.append("circle")
				.attr("cx", function(d) {
					return xScale(d.ts);
				})
				.attr("cy", function(d) {
					return h - yScale(d.value);
				})
				.attr("r", 2)
				.attr("data-id", function(d){
					return d._id;
				});

		//Update…
		data.attr("fill", function(d) {
					return colorCodesForClasses[classForValue(d)].solid;
				})
			.attr("cx", function(d) {
				return xScale(d.ts);
			})
			.attr("cy", function(d) {
				return h - yScale(d.value);
			});

		//Exit…
		data.exit()
			.remove();

		 
		//
		// Put one big colored bar, representing latest data point
		//

		var bar = svg.selectAll("rect.data").data(datasetLatest, getKey);

		bar.enter()
			.append("rect")
				.classed("data", true)
				.attr("x", 0)
				.attr("y", function(d) {
					return h - yScale(d.value) - (yScale(100) * 0.025);
				})
				.attr("rx", "5")
				.attr("ry", "5")
				.attr("width", w)
				//.attr("height", function(d) { return yScale(d.value); })
				.attr("height", "5%")

				.attr("fill", function(d) {
					return colorCodesForClasses[classForValue(d)].transparent;
				})
				.attr("data-id", function(d){
					return d._id;
				});

		bar.exit()
			.remove();

		//
		// Update label
		//
		var labels = svg.selectAll("text").data(datasetLatest, getKey);

		//Enter…
		labels.enter()
			.append("text")
			.text(function(d) {
				return "" + d.value.toPrecision(3) + " hurrahs";
			})
			.attr("text-anchor", "left")
			.attr("x", "1.5em")
			.attr("y", "1.5em")						
			.attr("font-family", "sans-serif")
			.attr("font-size", "2em")
			.attr("fill", function(d) {
				return colorCodesForClasses[classForValue(d)].text;
			});

		//Exit…
		labels.exit()
			.remove();
	});
};