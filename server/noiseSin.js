
TwoPI = Math.PI * 2; 

NoisySource = function(nominal, waveforms, nominalPeriod, maxPeriodVariance, maxAmplitude) {
	/* 
	Define a nominal, 'normal' value.
	Define how many sine waves may perturb it, their frequency, and their variability (0=none, 1=100%).
	Define the max amplitude of any sine wave.

	This will set up the number of wave forms specified, with an amplitude <= maxAmplitude, at random initial states, etc
	*/

	this.nominal = nominal;
	this.waveforms = [];
	this.startTime = new Date().getTime()/1000;

	if (typeof waveforms === typeof []) {
		// we were provided specific waveforms
		console.log("provided array of waveforms");
		o = this;
		waveforms.forEach(function(wf) {
			var w = {
				p   : wf[0],  // period
				amp : wf[1],  // amplitude
				startPhase : 0
			}
			if (wf.length > 2) {
				w.startPhase = wf[2];  // Startphase is optional
			}
			o.waveforms.push(w);
			console.log(JSON.stringify(w));
		})
	} else if (typeof waveforms === typeof 1) {
		// we were provided a number.  We'll create that many waveforms randomly.
		console.log("building array of waveforms");
		for (var i = 0 ; i < waveforms; i++) {
			var period = (Math.random() * 2 - 1) * (maxPeriodVariance * nominalPeriod) + nominalPeriod;
			var w = {
				p   : period,
				amp : Math.random() * maxAmplitude,
				startPhase : TwoPI / waveforms * i
			}
			this.waveforms.push(w);
			console.log(JSON.stringify(w));
		}
	}

}

NoisySource.prototype.update = function(debug) {
	var now = new Date().getTime()/1000;
	var runtime = now - this.startTime;

	var values = [];
	var value = this.nominal;

	this.waveforms.forEach(function(w) {
		var cycles = runtime/w.p;
		var currentPhase = cycles * TwoPI + w.startPhase;
		var influence = Math.sin(currentPhase) * w.amp;
		values.push(influence);
		value = value + influence
	});
	//console.log(values);

	return value;
}

plot = function (value, min, max, size) {
	var range = max - min;
	var adjValue = value - min;
	var output = "";
	for (var i = 0 ; i < size * adjValue/range ; i++) {
		output += " ";
	}
	output += "*";
	for (var i = size * adjValue/range + 1 ; i <= range ; i++) {
		output += " ";
	}
	return output;
}

/*
var n = new NoisySource(50, 5, .1, .05, 5);

var x = 400;
function update() {
	var v = n.update();
	console.log(plot(v, 0,100, 100) + " : %d", v.toPrecision(2));
	x--;
	if (x) setTimeout(update, 250)
}

update()
*/