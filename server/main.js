
Meteor.startup(function () {
	//var n = new NoisySource(50, 2, 10, .05, 5);
	var n1 = new NoisySource(50,
		[
			[120,   4,   0],
			[120.5, 4,   0],
			[45,    2.5, 0],
			[45.1,  0.5, 0],
		]);

	Meteor.setInterval(
		function() {

			var v = n1.update();
			//console.log(plot(v, 0, 100, 100) + " : %d", v.toPrecision(2));
			DataPoints.insert({ts: new Date(), channel:'sample', value: v})
		}, 500);

	Meteor.setInterval(
		function() {
			var cutoff = new Date( new Date() - 3600000);
			console.log("Removing data older than " + cutoff);
			console.log(DataPoints.remove({ts: {$lt : cutoff}}) + " records removed.");
		}, 30000);


});

Meteor.publish('datapoints', function(records) {
	return DataPoints.find({}, {sort:{ts:-1}, limit:records});
})