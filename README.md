# Data Visualization Toy #

This toy demonstrates several things:

* [Meteor](https://www.meteor.com/) - JavaScript Application Development stack.  Meteor utilizes Node.js and MongoDB on the server side, 'Mini-mongo', Blaze, and Spacebars (similar to Handlebars, but specific to Meteor's reactive engine, Blaze) on the client side, and ties them together with a bespoke [DDP](https://www.meteor.com/ddp) protocol which rides on websockets.  The end result is a server which can push live updates, particularly data updates, to the client.
* Some light [Twitter Bootstrap](http://getbootstrap.com/).  Not pushing any envelopes here, but it's a simple way to make a presentable and responsive site.
* [D3.js](http://d3js.org/) - Javascript "Data Driven Document" toolset.  Or, as it's more commonly known as/for: A data visualization toolset.  Making the <svg> that D3 is using behave responsively is not as trivial as the rest of the page, but I found a good example of how to do that on [this site](http://www.brendansudol.com/posts/responsive-d3/).

Those are the broad strokes.  What's happening here is:

* Server side is running some code about twice per second that generates some data and inserts it into a MongoDB collection. The data comes from a "noise" generator - currently several sine wave forms of different frequencies summed together.
* Server side is also publishing a recordset to each connected client with the last n records in that collection (n=300, as I write this, but it may be reduced).  Because of Meteor's reactive nature, new documents added to that collection are pushed to subscribing clients over DDP. 
* When the client template is ready (basically, moments after initial page load), it sets up a <svg> for use with D3, and subscribes to the recordset being published by the server.  It then sets up a block of code which will process new data as it arrives.  This is done in a Tracker.autorun block, which will know when new data has arrived via the subscription, and then re-run the block of code.						
* Within the autorun block, the [current n] records are fetched into a JS array, and then used as a dataset to drive D3.  Actually, there are three datasets generated - one for painting the datapoints, one for the movable green/yellow/red band which tracks the newest datapoint (it's also used for the value displayed in graph in upper left), and one for drawing the path segments to connect the dots - this is a transformation of the set used for the points, which includes the x and y values of the previous datapoint.

Although the animation of the chart 'steps', as it were, it's worth pointing out that the client is NOT polling (actually, if the network topology between the client and the server blocks the DDP, it may fall back to a polling mode), that stepping is the interval of data being pushed into the database, and/or from the server to the client - whatever the bottleneck is.  On a LAN, the insertion interval is likely the bottleneck.  On the Internet, network latency is likely the bottleneck.

This project *may* be viewable [here](http://data-visualization-toy-jsibre.meteor.com/)

To run this project locally, install meteor, clone this repo, cd in the the working directory of this project, and type "meteor".